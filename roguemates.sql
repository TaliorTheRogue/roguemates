-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : dim. 16 juin 2024 à 22:56
-- Version du serveur : 8.0.36-0ubuntu0.22.04.1
-- Version de PHP : 8.1.2-1ubuntu2.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `roguemates`
--

-- --------------------------------------------------------

--
-- Structure de la table `colocations`
--

CREATE TABLE `colocations` (
  `id` int NOT NULL,
  `name` varchar(35) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zip_code` int NOT NULL,
  `city` varchar(255) NOT NULL,
  `landlord_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `colocations`
--

INSERT INTO `colocations` (`id`, `name`, `address`, `zip_code`, `city`, `landlord_id`) VALUES
(1, 'MyFirstColoc', '65, Power Street', 8040, 'Los Santos', 1);

-- --------------------------------------------------------

--
-- Structure de la table `landlords`
--

CREATE TABLE `landlords` (
  `id` int NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `landlords`
--

INSERT INTO `landlords` (`id`, `first_name`, `last_name`, `email`, `password`) VALUES
(1, 'Logan', 'Foster', 'logan.foster@discord.gg', '123456');

-- --------------------------------------------------------

--
-- Structure de la table `landlord_messages`
--

CREATE TABLE `landlord_messages` (
  `id` int NOT NULL,
  `message_text` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `colocation_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `landlord_messages`
--

INSERT INTO `landlord_messages` (`id`, `message_text`, `colocation_id`) VALUES
(1, 'Visite prévue du plombier pour la douche le 22/01/2024', 1);

-- --------------------------------------------------------

--
-- Structure de la table `payments`
--

CREATE TABLE `payments` (
  `id` int NOT NULL,
  `category` enum('Courses','Repas','Loisirs','Administratif') NOT NULL,
  `title` varchar(255) NOT NULL,
  `money_sum` decimal(10,0) NOT NULL,
  `description` text NOT NULL,
  `creation_date` date NOT NULL,
  `limit_date` date NOT NULL,
  `creator_id` int NOT NULL,
  `user_id` int NOT NULL,
  `done` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `payments`
--

INSERT INTO `payments` (`id`, `category`, `title`, `money_sum`, `description`, `creation_date`, `limit_date`, `creator_id`, `user_id`, `done`) VALUES
(1, 'Repas', 'Domino\'s', '25', 'Soirée pizza du 13/06', '2024-06-15', '2024-06-30', 2, 1, 0),
(2, 'Loisirs', 'Karaoké', '20', 'Place soirée Karaoké', '2024-06-14', '2024-06-20', 1, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `post_its`
--

CREATE TABLE `post_its` (
  `id` int NOT NULL,
  `title` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `user_author_id` int NOT NULL,
  `creation_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `post_its`
--

INSERT INTO `post_its` (`id`, `title`, `message`, `user_author_id`, `creation_date`) VALUES
(1, 'Bienvenue', 'Bienvenue dans la coloc\' !', 1, '2024-06-14'),
(2, 'Date Night', 'J\'aurais besoin de l\'appart mardi au soir (le 18/06)', 2, '2024-06-14');

-- --------------------------------------------------------

--
-- Structure de la table `tasks`
--

CREATE TABLE `tasks` (
  `id` int NOT NULL,
  `category` enum('Ménage','Courses','Repas','Administratif') NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `creation_date` date NOT NULL,
  `limit_date` date NOT NULL,
  `creator_id` int NOT NULL,
  `user_id` int NOT NULL,
  `done` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `tasks`
--

INSERT INTO `tasks` (`id`, `category`, `title`, `description`, `creation_date`, `limit_date`, `creator_id`, `user_id`, `done`) VALUES
(1, 'Ménage', 'Aspirateur', 'Passer l\'aspirateur dans le salon', '2024-06-14', '2024-06-18', 1, 1, 0),
(2, 'Courses', 'Pizzas', 'Aller acheter des pizzas', '2024-06-14', '2024-06-20', 1, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `first_name` varchar(35) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `pronoun` varchar(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `profile_pic` varchar(255) NOT NULL,
  `colocation_id` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `pronoun`, `email`, `password`, `description`, `profile_pic`, `colocation_id`) VALUES
(1, 'Alex', 'Taylor', 'He/Him', 'alex.taylor@discord.gg', 'Agent45', 'Hi ! I\'m 23 years old and I worked as a police officer at LSPD - Mission Row Precint.', 'https://icon-library.com/images/icon-for-profile/icon-for-profile-27.jpg', 1),
(2, 'Jake', 'Crimson', 'He/Him', 'jake.crimson@discord.gg', '123456', '\"La justice n\'attend pas !\"', '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_sessions`
--

CREATE TABLE `user_sessions` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `session_token` varchar(255) NOT NULL,
  `expires_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `user_sessions`
--

INSERT INTO `user_sessions` (`id`, `user_id`, `session_token`, `expires_at`) VALUES
(8, 1, '1738cb970ac8bde6beac5939b7684be7403d05279996f632035676606b64ee36', '2024-06-20 20:53:04'),
(9, 1, '78ce7bf1db97a8cf4b8a33decd661134aa6461604733ca1cec34d605057ea5bc', '2024-06-20 20:57:03'),
(10, 1, 'f7d5f4dc7913e0b36f1e76ef7ae4e1f9d4b9947e4f61ab8cf6d25516c8e9b109', '2024-06-20 22:27:39'),
(11, 1, 'ccee79e19f1211f9c572d4fbf7e04f51ee957b5ddcc8f6533ecc26e621cc77da', '2024-06-21 11:38:11'),
(12, 1, '968715d5bc039fe7a42a266012c29a861b2a4cd826795af459683e8e2e9ce97a', '2024-06-21 11:53:12'),
(13, 1, '05541be7d8661c1dad6fa1fb460c857182c9f8c8c7a0e2700008b6904ac3ab5b', '2024-06-21 11:55:55'),
(14, 1, '58dadc1bf89e2f119ab97115f08788ea69c1bd7b6664271db52022b2336d854b', '2024-06-21 12:13:22'),
(15, 1, '87d2c146bb7715ebb82326692dae18f5498622f25faa19c79d81510de5caa0be', '2024-06-21 13:39:09'),
(16, 2, '52831cfce3b5dc7780d47bc510b03b08d199af2775dc1660bfafd37d8039f62f', '2024-06-21 13:58:46'),
(17, 1, '54b43899b42f5ccbef6191c0e8e98a85e165ed082a1cbfa8a7270526be225a64', '2024-06-21 19:24:34'),
(18, 1, '86d8ac693b0c772c31d7c161c767131cf32c278f9beac7fb993449ba20c6c5f4', '2024-06-22 14:56:41'),
(19, 1, 'b8108dc6d02dbbe3c9244ad40bebd74977905179023ca1e0e3681b8f5ecd5ac2', '2024-06-22 15:04:54'),
(20, 1, 'ddb236d77d0253a05bedd59d3f516699282e57bb71920dae0e5c7e56e3ae3a43', '2024-06-22 20:44:42'),
(21, 1, '9bb077cdf4ac8da14b142135cd7056e6437a40404fafc424b13ac3ee17336f13', '2024-06-23 14:35:24'),
(22, 1, '8621f5742fffa996e30f7233bbb92f539acef1e93ab8bd8af8d4d01488436ad0', '2024-06-23 16:21:46'),
(23, 2, '4ba11d476031160eec227d3a1669c0860c19a391283807729c7692c1ce4e4a78', '2024-06-23 20:52:44'),
(24, 1, '1f8871bf9d119734db02cad37eeca387d96553e2d0aa7151cf942ce3a6e82b3f', '2024-06-23 20:53:00'),
(25, 1, '2081d1513c3061dca134b714f39cefa8868e0675a6043cb587cefa504ebd22ab', '2024-06-23 20:53:32'),
(26, 2, '5cfccedd0071eb5d54fe0f2ef143658b2a96e0dfad64b87f6e60ef5cff5642b2', '2024-06-23 20:53:43');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `colocations`
--
ALTER TABLE `colocations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `landlords`
--
ALTER TABLE `landlords`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `landlord_messages`
--
ALTER TABLE `landlord_messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `post_its`
--
ALTER TABLE `post_its`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `email` (`email`);

--
-- Index pour la table `user_sessions`
--
ALTER TABLE `user_sessions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `colocations`
--
ALTER TABLE `colocations`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `landlords`
--
ALTER TABLE `landlords`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `landlord_messages`
--
ALTER TABLE `landlord_messages`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `post_its`
--
ALTER TABLE `post_its`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `user_sessions`
--
ALTER TABLE `user_sessions`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
