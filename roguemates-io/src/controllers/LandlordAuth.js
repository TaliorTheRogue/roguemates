import viewLandlordLogin from '../views/landlord/landlordlogin';
import viewLandlordRegister from '../views/landlord/landlordregister';
import viewFooter from '../views/footer';
import logo from '../assets/img/RogueMates Logo.png';

const LandlordAuth = class {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;

    this.run();
  }

  renderLogin() {
    return (`
      ${viewLandlordLogin()}
    `);
  }

  renderRegister() {
    return (`
      ${viewLandlordRegister()}
    `);
  }

  render() {
    return (`
      <section id="landlord-logform">
        <div class="homepage-login d-flex flex-column align-items-center mb-3">
          <div class="logo-space d-flex flex-column justify-content-center align-items-center">
            <img class="logo" src=${logo}>
            <h1>RogueMates</h1>
            <h2>"La colocation en toute simplicité"</h2>
          </div>
        <div class="tab-form d-flex justify-content-around w-50">
          <div id="landlord-login-tab" class="tab login-tab p-3 active">CONNEXION</div>
          <div id="landlord-register-tab" class="tab signup-tab p-3">INSCRIPTION</div>
        </div>
        ${this.renderLogin()}
        <a class="m-3" href="/">Vous êtes colocataire ? Cliquez ici !</a>
      <section>
      ${viewFooter()}
    `);
  }

  async run() {
    this.el.innerHTML = this.render();
  }
};

export default LandlordAuth;
