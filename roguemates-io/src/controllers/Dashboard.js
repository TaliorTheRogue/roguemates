import Cookies from 'js-cookie';
import viewNav from '../views/user/dashboard/nav';
import viewFooter from '../views/footer';
import viewLandlordMessage from '../views/user/dashboard/landlordmessage';
import viewTasks from '../views/user/dashboard/usertasks';
import viewPayments from '../views/user/dashboard/userpayments';
import viewPostIts from '../views/user/dashboard/postits';
import viewShoppingList from '../views/user/dashboard/shoppinglist';
import viewCalendar from '../views/user/dashboard/usercalendar';

const Dashboard = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.user = null;
    this.tasks = [];

    this.run();
  }

  async fetchUserInfo() {
    try {
      const userId = Cookies.get('userId');
      const sessionToken = Cookies.get('sessionToken');
      const response = await fetch(`http://localhost/user/${userId}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${sessionToken}`
        },
        credentials: 'include'
      });

      const data = await response.json();

      if (data.status === 'success') {
        this.user = data.user;
        this.colocation = data.user.colocation;
        console.log(data);
      } else {
        console.error('Error fetching user info:', data.message);
      }
    } catch (error) {
      console.error('Error fetching user info:', error);
    }
  }

  async fetchUserTasks() {
    try {
      const userId = Cookies.get('userId');
      const sessionToken = Cookies.get('sessionToken');
      const response = await fetch(`http://localhost/user/${userId}/tasks`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${sessionToken}`
        },
        credentials: 'include'
      });

      const data = await response.json();
      console.log('Response:', response);
      console.log('Data:', data);

      if (data.status === 'success') {
        this.tasks = data.tasks;
      } else {
        console.error('Error fetching tasks:', data.message);
      }
    } catch (error) {
      console.error('Error fetching tasks:', error);
    }
  }

  async fetchUserPayments() {
    try {
      const userId = Cookies.get('userId');
      const sessionToken = Cookies.get('sessionToken');
      const response = await fetch(`http://localhost/user/${userId}/payments`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${sessionToken}`
        },
        credentials: 'include'
      });

      const data = await response.json();
      console.log('Response:', response);
      console.log('Data:', data);

      if (data.status === 'success') {
        this.payments = data.payments;
      } else {
        console.error('Error fetching tasks:', data.message);
      }
    } catch (error) {
      console.error('Error fetching tasks:', error);
    }
  }

  logoutEventListener() {
    document.querySelector('#logout-button').addEventListener('click', async (event) => {
      event.preventDefault();

      try {
        Cookies.remove('sessionToken');
        Cookies.remove('userId');
        window.location.href = '/';
      } catch (error) {
        console.error('Error:', error);
      }
    });
  }

  render() {
    return (`
      ${viewNav(this.user.first_name, this.colocation.name)}
      <div class="dashboard p-3 mb-3">
        <div class="landlord-message">
          ${viewLandlordMessage()}
        </div>
        <div class="gen-overview d-flex row">
          <div class="user-commands d-flex justify-content-evenly flex-column col-2 p-3">
            <button id="profile-button" class="btn btn-primary btn-square card-shadow d-flex flex-column justify-content-around w-100" href="/profile" role="button">
              <p class="fs-5 m-0">Mon profil</p>
              <i class="fa-solid fa-user-pen fa-2xl"></i>
            </button>
            <button id="coloc-button" class="btn btn-primary btn-square card-shadow d-flex flex-column justify-content-around w-100" href="/colocation" role="button">
              <p class="fs-5 m-0">Ma coloc'</p>
              <i class="fa-solid fa-house-chimney fa-2xl"></i>
            </button>
            <button id="logout-button" class="btn btn-primary btn-square card-shadow d-flex flex-column justify-content-around w-100" role="button">
              <p class="fs-5 m-0">Me déconnecter</p>
              <i class="fa-solid fa-user-lock fa-2xl"></i>
            </button>
          </div>
          <div class="user-overview col-7">
            <div class="user-shortcuts d-flex justify-content-between">
              ${viewTasks(this.tasks)}
              ${viewPayments(this.payments)}
              ${viewShoppingList()}
            </div>
            <div class="user-postits">
              ${viewPostIts()}
            </div>
          </div>
          <div class="user-calendar p-3 col-3">
            ${viewCalendar()}
          </div>
        </div>      
      </div>

      <!-- Profile Modal -->
      <div id="profile-modal" class="modal">
        <div class="modal-content">
          <span class="close" id="close-profile-modal"><i class="fa-regular fa-circle-xmark"></i></span>
          <h2>MON PROFIL</h2>
          <p>PRONOMS: ${this.user.pronoun}</p>
          <p>NOM: ${this.user.first_name} ${this.user.last_name}</p>
          <p>DESCRIPTION: ${this.user.description}</p>
          <p>EMAIL: ${this.user.email}</p>
        </div>
      </div>

      <!-- Colocation Modal -->
      <div id="coloc-modal" class="modal">
        <div class="modal-content">
          <span class="close" id="close-coloc-modal"><i class="fa-regular fa-circle-xmark"></i></span>
          <h2>MA COLOCATION</h2>
          <div class="d-flex justify-content-between m-3 row">
            <div class="coloc-infos col-6">
              <p>NOM DE LA COLOCATION: ${this.colocation.name}</p>
              <p>ADRESSE: ${this.colocation.address}</p>
              <p>CODE POSTAL: ${this.colocation.zip_code}</p>
              <p>VILLE: ${this.colocation.city}</p>
            </div>
            <div class="landlord-contact col-6">
              <p>PROPRIETAIRE :</p>
              <form class="landlord-message">
                <label for="message" class="form-label">Message à mon propriétaire</label>
                <textarea class="form-control" id="messagetext" rows="3"></textarea>
                <button type="submit" class="btn btn-primary m-3 justify-self-center">ENVOYER</button>
              </form>
            </div>
          </div>
        </div>
      </div>

      ${viewFooter()}
    `);
  }

  // Event Listeners to display associated Modals
  setupModalEventListeners() {
    // Profile Display
    const profileButton = document.querySelector('#profile-button');
    const profileModal = document.querySelector('#profile-modal');
    const closeProfileModal = document.querySelector('#close-profile-modal');
    // Flatshare Details display
    const colocButton = document.querySelector('#coloc-button');
    const colocModal = document.querySelector('#coloc-modal');
    const closeColocModal = document.querySelector('#close-coloc-modal');
    // All Tasks display
    const tasksButton = document.querySelector('#display-tasks');
    const tasksModal = document.querySelector('#tasks-modal');
    const closeTasksModal = document.querySelector('#close-tasks-modal');
    // All Payments Display
    const paymentsButton = document.querySelector('#display-payments');
    const paymentsModal = document.querySelector('#payments-modal');
    const closePaymentsModal = document.querySelector('#close-payments-modal');

    profileButton.addEventListener('click', () => {
      profileModal.style.display = 'block';
    });

    closeProfileModal.addEventListener('click', () => {
      profileModal.style.display = 'none';
    });

    colocButton.addEventListener('click', () => {
      colocModal.style.display = 'block';
    });

    closeColocModal.addEventListener('click', () => {
      colocModal.style.display = 'none';
    });

    tasksButton.addEventListener('click', () => {
      tasksModal.style.display = 'block';
    });

    closeTasksModal.addEventListener('click', () => {
      tasksModal.style.display = 'none';
    });

    paymentsButton.addEventListener('click', () => {
      paymentsModal.style.display = 'block';
    });

    closePaymentsModal.addEventListener('click', () => {
      paymentsModal.style.display = 'none';
    });

    window.addEventListener('click', (event) => {
      if (event.target === profileModal) {
        profileModal.style.display = 'none';
      }
      if (event.target === colocModal) {
        colocModal.style.display = 'none';
      }
      if (event.target === tasksModal) {
        tasksModal.style.display = 'none';
      }
      if (event.target === paymentsModal) {
        paymentsModal.style.display = 'none';
      }
    });
  }

  async run() {
    await this.fetchUserInfo();
    await this.fetchUserTasks();
    await this.fetchUserPayments();
    this.el.innerHTML = this.render();
    this.logoutEventListener();
    this.setupModalEventListeners();
  }
};

export default Dashboard;
