import Cookies from 'js-cookie';
import viewLogin from '../views/user/login';
import viewFooter from '../views/footer';

const Login = class {
  constructor(params) {
    this.el = document.querySelector('#root');
    this.params = params;

    this.run();
  }

  submitEventListener() {
    document.querySelector('#user-login-form').addEventListener('submit', async (event) => {
      event.preventDefault();

      const email = document.querySelector('#user-email').value;
      const password = document.querySelector('#user-password').value;

      try {
        const response = await fetch('http://localhost/login', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify({ email, password })
        });

        const data = await response.json();

        if (data.status === 'success') {
          Cookies.set('sessionToken', data.sessionToken, { expires: 7, sameSite: 'Lax', secure: true });
          Cookies.set('userId', data.userId, { expires: 7, sameSite: 'Lax', secure: true });
          window.location.href = '/dashboard';
        } else {
          alert('Login failed: '.data.message);
        }
      } catch (error) {
        console.error('Error:', error);
      }
    });
  }

  render() {
    return (`
      ${viewLogin()}
      ${viewFooter()}
    `);
  }

  run() {
    this.el.innerHTML = this.render();
    this.submitEventListener();
  }
};

export default Login;
