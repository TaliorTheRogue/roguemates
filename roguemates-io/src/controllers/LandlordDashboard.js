import viewNav from '../views/landlord/dashboard/nav';
import viewColocOverview from '../views/landlord/dashboard/colocationlist';
import viewColocMessages from '../views/landlord/dashboard/receivedmessage';
import viewFooter from '../views/footer';

const LandlordDashboard = class {
  constructor() {
    this.el = document.querySelector('#root');

    this.run();
  }

  render() {
    return (`
      ${viewNav()}
      <div class="landlord-dashboard p-3 mb-3">
        <div class="gen-overview d-flex row">
          <div class="user-commands d-flex align-content-center justify-content-evenly flex-column col-2 p-3">
            <a class="btn landlord-btn btn-primary btn-square card-shadow d-flex flex-column justify-content-around mb-3" href="#" role="button">
              <p class="fs-5 m-0">Créer une colocation</p>
              <i class="fa-solid fa-house-chimney fa-2xl"></i>
            </a>
            <a class="btn landlord-btn btn-primary btn-square card-shadow d-flex flex-column justify-content-around mb-3" href="#" role="button">
              <p class="fs-5 m-0">Ajouter un locataire'</p>
              <i class="fa-solid fa-house-chimney-user fa-2xl"></i>
            </a>
            <a class="btn landlord-btn btn-primary btn-square card-shadow d-flex flex-column justify-content-around mb-3" href="#" role="button">
              <p class="fs-5 m-0">Mon profil</p>
              <i class="fa-solid fa-crown fa-2xl"></i>
            </a>
            <a class="btn landlord-btn btn-primary btn-square card-shadow d-flex flex-column justify-content-around w-100" href="/logout" role="button">
              <p class="fs-5 m-0">Me déconnecter</p>
              <i class="fa-solid fa-user-lock fa-2xl"></i>
            </a>
          </div>
          <div class="landlord-overview card-shadow m-3 col-9">
            <h3 class="m-3"><i class="fa-solid fa-house-chimney"></i>  Mes colocations</h3>
            ${viewColocOverview()}
            <h3 class="m-3"><i class="fa-solid fa-message"></i>  Mes messages</h3>
            ${viewColocMessages()}
          </div>
        </div>      
      </div>
      ${viewFooter()}
    `);
  }

  run() {
    this.el.innerHTML = this.render();
  }
};

export default LandlordDashboard;
