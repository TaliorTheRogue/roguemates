import viewFooter from '../views/footer';
import Logo from '../assets/img/RogueMates Logo.png';

const Quatre04 = class {
  constructor() {
    this.el = document.querySelector('#root');
    this.run();
  }

  render() {
    return (`
      <div class="logo-space d-flex flex-column justify-content-center align-items-center">
        <img class="logo m-2" src=${Logo} alt="RogueMates Logo">
        <h1>Erreur 404</h1>
        <h2 class="fst-italic">La page à laquelle vous souhaitez accéder, n'existe pas ou plus.</h2>
        <a href="/">Revenir à la page d'accueil<a>
      </div>
      ${viewFooter}
    `);
  }

  run() {
    this.el.innerHTML = this.render();
  }
};

export default Quatre04;
