import Router from './Router';
import Login from './controllers/Login';
import Dashboard from './controllers/Dashboard';
import LandlordAuth from './controllers/LandlordAuth';
import LandlordDashboard from './controllers/LandlordDashboard';
import './index.scss';

const routes = [{
  url: '/',
  controller: Login
},
{
  url: '/dashboard',
  controller: Dashboard
},
{
  url: '/landlordauth',
  controller: LandlordAuth
},
{
  url: '/landlorddashboard',
  controller: LandlordDashboard
}
];

new Router(routes);
