export default () => (`
  <div class="colocations-overview p-3">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">Nom de la coloc'</th>
          <th scope="col">Adresse</th>
          <th scope="col">Code Postal</th>
          <th scope="col">Ville</th>
          <th scope="col">Nombre d'habitants</th>
          <th scope="col">Action</th>
        </tr>
      </thead>
      <tbody class="colocations-list overflow-auto">
        <tr>
          <th scope="row">MyFirstColoc'</th>
          <td>65, Power Street</td>
          <td>8060</td>
          <td>Los Santos</td>
          <td>4</td>
          <td><i class="fa-solid fa-circle-info"></i>           <i class="fa-solid fa-envelope"></i>          <i class="fa-solid fa-user-pen"></i></td>
        </tr>
        <tr>
          <th scope="row">MySecondColoc'</th>
          <td>65, Power Street</td>
          <td>8060</td>
          <td>Los Santos</td>
          <td>4</td>
          <td><i class="fa-solid fa-circle-info"></i>           <i class="fa-solid fa-envelope"></i>          <i class="fa-solid fa-user-pen"></i></td>
        </tr>
        <tr>
          <th scope="row">MyThirdColoc'</th>
          <td>65, Power Street</td>
          <td>8060</td>
          <td>Los Santos</td>
          <td>4</td>
          <td><i class="fa-solid fa-circle-info"></i>           <i class="fa-solid fa-envelope"></i>          <i class="fa-solid fa-user-pen"></i></td>
        </tr>
        <tr>
          <th scope="row">MyThirdColoc'</th>
          <td>65, Power Street</td>
          <td>8060</td>
          <td>Los Santos</td>
          <td>4</td>
          <td><i class="fa-solid fa-circle-info"></i>           <i class="fa-solid fa-envelope"></i>          <i class="fa-solid fa-user-pen"></i></td>
        </tr>
        <tr>
          <th scope="row">MyThirdColoc'</th>
          <td>65, Power Street</td>
          <td>8060</td>
          <td>Los Santos</td>
          <td>4</td>
          <td><i class="fa-solid fa-circle-info"></i>           <i class="fa-solid fa-envelope"></i>          <i class="fa-solid fa-user-pen"></i></td>
        </tr>
      </tbody>
    </table>
  </div>
`);
