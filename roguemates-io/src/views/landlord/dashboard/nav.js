import logo from '../../../assets/img/RogueMates Logo.png';

export default () => (`
  <nav class="navbar bg-body-tertiary mb-1 ps-4 pe-4">
    <div class="container-fluid">
      <a class="navbar-brand" href="#">
        <img src=${logo} alt="Logo" width="50" height="50" class="d-inline-block align-text-center text-light">
      </a>
      <h1 class="text-center mb-4">Bienvenue, *insérez nom de propriétaire* !</h1>
      <h3>RogueMates</h3>
    </div>
  </nav>
`);
