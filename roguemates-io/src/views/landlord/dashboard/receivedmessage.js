export default () => (`
  <div class="card-body ps-0 pe-0 ms-3 me-3 overflow-auto d-flex">
    <div class="card postit h-100 me-3" style="min-width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">Problème douche</h5>
        <h6 class="card-subtitle mb-2 ms-2 fst-italic">de Coloc 1 dans Nom de coloc</h6>
        <p class="card-text">"Nous avons un problème sur le pommeau de la douche."</p>
        <p class="message-date text-end fst-italic">01/01/2024</p>
        <button>Marquer comme géré</button>
      </div>
    </div>
    <div class="card postit h-100 me-3" style="min-width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">Problème douche</h5>
        <h6 class="card-subtitle mb-2 ms-2 fst-italic">de Coloc 1 dans Nom de coloc</h6>
        <p class="card-text">"Nous avons un problème sur le pommeau de la douche."</p>
        <p class="message-date text-end fst-italic">01/01/2024</p>
        <button>Marquer comme géré</button>
      </div>
    </div>
    <div class="card postit h-100 me-3" style="min-width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">Problème douche</h5>
        <h6 class="card-subtitle mb-2 ms-2 fst-italic">de Coloc 1 dans Nom de coloc</h6>
        <p class="card-text">"Nous avons un problème sur le pommeau de la douche."</p>
        <p class="message-date text-end fst-italic">01/01/2024</p>
        <button>Marquer comme géré</button>
      </div>
    </div>
    <div class="card postit h-100 me-3" style="min-width: 18rem;">
      <div class="card-body">
        <h5 class="card-title">Problème douche</h5>
        <h6 class="card-subtitle mb-2 ms-2 fst-italic">de Coloc 1 dans Nom de coloc</h6>
        <p class="card-text">"Nous avons un problème sur le pommeau de la douche."</p>
        <p class="message-date text-end fst-italic">01/01/2024</p>
        <button>Marquer comme géré</button>
      </div>
    </div>
  </div>
`);
