export default () => (`
  <div id="landlord-register" class="signup-form d-flex flex-column justify-content-center w-50 h-50">
    <div class="m-3 d-flex">
      <label for="firstname" class="form-label w-100 text-center">PRENOM :</label>
      <input type="firstname" class="form-control" id="firstname" placeholder="Prénom">
      <label for="lastname" class="form-label w-100 text-center">NOM :</label>
      <input type="lastname" class="form-control" id="lastname" placeholder="Nom de famille">
    </div>
    <div class="m-3">
      <label for="email" class="form-label w-100 text-center">ADRESSE MAIL</label>
      <input type="email" class="form-control" id="email" placeholder="name@example.com">
    </div>
    <div class="m-3">
      <label for="password" class="form-label w-100 text-center">MOT DE PASSE :</label>
      <input type="password" class="form-control" id="password" placeholder="Mot de passe">
      <label for="confirm-password" class="form-label w-100 text-center">CONFIRMER MOT DE PASSE :</label>
      <input type="password" class="form-control" id="confirm-password" placeholder="Confirmation Mot de passe">
    </div>
      <button type="submit" class="btn btn-primary m-3 justify-self-center" action="#">INSCRIPTION</button>
    </div>
  </div>
  <a class="m-3" href="/">Vous êtes colocataire ? Cliquez ici !</a>
`);
