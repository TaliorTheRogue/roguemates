export default () => (`
    <div id="landlord-login" class="login-form d-flex flex-column justify-content-center w-50 h-50 active">
      <div class="m-3">
        <label for="email" class="form-label w-100 text-center">ADRESSE MAIL</label>
        <input type="email" class="form-control" id="email" placeholder="name@example.com">
      </div>
      <div class="m-3">
        <label for="password" class="form-label w-100 text-center">MOT DE PASSE</label>
        <input type="password" class="form-control" id="password" placeholder="Mot de passe">
      </div>
      <button type="submit" class="btn btn-primary m-3 justify-self-center" action="#">CONNEXION</button>
    </div>
`);
