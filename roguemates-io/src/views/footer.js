export default () => (`
  <footer class="d-flex align-items-center justify-content-center">
    <div class="links"></div>
    <h5 class="p-2">Powered by RogueCorp</h3>
    <div class="socials"></div>
  </footer>
`);
