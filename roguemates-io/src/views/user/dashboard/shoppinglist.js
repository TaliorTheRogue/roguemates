export default () => (`
  <div class="card user-card card-shadow m-3">
    <h5 class="card-header"><i class="fa-solid fa-cart-shopping"></i>  Liste de course</h5>
    <div class="card-body overflow-auto">
      <ul class="list-group overflow-auto">
        <li class="list-group-item">
          <input class="form-check-input me-1" type="checkbox" value="" id="firstCheckbox">
          <label class="form-check-label" for="firstCheckbox">First checkbox</label>
        </li>
        <li class="list-group-item">
          <input class="form-check-input me-1" type="checkbox" value="" id="secondCheckbox">
          <label class="form-check-label" for="secondCheckbox">Second checkbox</label>
        </li>
        <li class="list-group-item">
          <input class="form-check-input me-1" type="checkbox" value="" id="thirdCheckbox">
          <label class="form-check-label" for="thirdCheckbox">Third checkbox</label>
        </li>
      </ul>
    </div>
    <a href="#" class="btn btn-primary ms-3 me-3 mb-3">Ajouter un article</a>
  </div>
`);
