export default () => (`
  <div class="card card-shadow postits m-3">
    <h5 class="card-header"><i class="fa-regular fa-note-sticky"></i>  Post-its</h5>
    <div class="card-body ps-0 pe-0 ms-3 me-3 overflow-auto d-flex">
      <div class="card postit h-100 me-3" style="min-width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Bienvenue !</h5>
          <h6 class="card-subtitle mb-2 ms-2 fst-italic">de Coloc 1</h6>
          <p class="card-text">"Bienvenue à tous dans la coloc !"</p>
          <p class="postit-date text-end fst-italic">01/01/2024</p>
        </div>
      </div>
      <div class="card postit h-100 me-3" style="min-width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Bienvenue !</h5>
          <h6 class="card-subtitle mb-2 ms-2 fst-italic">de Coloc 1</h6>
          <p class="card-text">"Bienvenue à tous dans la coloc !"</p>
          <p class="postit-date text-end fst-italic">01/01/2024</p>
        </div>
      </div>
      <div class="card postit h-100 me-3" style="min-width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Bienvenue !</h5>
          <h6 class="card-subtitle mb-2 ms-2 fst-italic">de Coloc 1</h6>
          <p class="card-text">"Bienvenue à tous dans la coloc !"</p>
          <p class="postit-date text-end fst-italic">01/01/2024</p>
        </div>
      </div>
      <div class="card postit h-100 me-3" style="min-width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Bienvenue !</h5>
          <h6 class="card-subtitle mb-2 ms-2 fst-italic">de Coloc 1</h6>
          <p class="card-text">"Bienvenue à tous dans la coloc !"</p>
          <p class="postit-date text-end fst-italic">01/01/2024</p>
        </div>
      </div>
      <div class="card postit h-100 me-3" style="min-width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Bienvenue !</h5>
          <h6 class="card-subtitle mb-2 ms-2 fst-italic">de Coloc 1</h6>
          <p class="card-text">"Bienvenue à tous dans la coloc !"</p>
          <p class="postit-date text-end fst-italic">01/01/2024</p>
        </div>
      </div>
    </div>
    <a href="#" class="btn btn-primary m-3">Laisser un post-it</a>
  </div>
`);
