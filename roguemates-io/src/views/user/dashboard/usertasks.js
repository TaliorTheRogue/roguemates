export default (userTasks) => (`
  <div class="card user-card card-shadow m-3">
    <h5 class="card-header"><i class="fa-regular fa-clipboard"></i>  Mes Tâches</h5>
    <div class="card-body overflow-auto">
      <ul class="list-group overflow-auto">
      ${userTasks.map((userTask) => `
        <li class="list-group-item">
          <input class="form-check-input me-1" type="checkbox" value="${userTask.id}" id="task-${userTask.id}" ${userTask.done ? 'checked' : ''}>
          <label class="form-check-label" for="task-${userTask.id}">${userTask.title}</label>
        </li>
      `).join('')}
      </ul>
    </div>
    <button id="display-tasks" class="btn btn-primary ms-3 me-3 mb-3">Voir les tâches</a>
  </div>

  <!-- Tasks Modal -->
    <div id="tasks-modal" class="modal">
      <div class="modal-content">
        <span class="close" id="close-tasks-modal"><i class="fa-regular fa-circle-xmark"></i></span>
        <h2>NOS TÂCHES</h2>
        <div class="d-flex justify-content-between m-3 row">
          <div class="tasks-table col-8">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Faite ?</th>
                  <th scope="col">Titre</th>
                  <th scope="col">Créée par</th>
                  <th scope="col">Date de création</th>
                  <th scope="col">Coloc' en charge</th>
                  <th scope="col">Date limite</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">
                  
                  </th>
                  <td>Mark</td>
                  <td>Otto</td>
                  <td>@mdo</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="task-create col-4">
            <p>NOUVELLE TÂCHE :</p>
            <form class="task-creation-form">
              <label for="title" class="form-label">Titre</label>
              <input class="form-control" id="new-task-title" rows="3">
              <label for="category" class="form-label">Catégorie</label>
              <select name="task-categories" class="form-control" id="task-category-select">
                <option value="">Choisissez une catégorie</option>
                <option value="menage">Ménage</option>
                <option value="courses">Courses</option>
                <option value="repas">Repas</option>
                <option value="administratif">Administratif</option>
              </select>
              <label for="description" class="form-label">Description</label>
              <textarea class="form-control" id="tasks-description" rows="3"></textarea>
              <label for="tasks-limit-date" class="form-label">Date limite</label>
              <input type="date" class="form-control" id="new-task-limit-date" rows="3">
              <label for="tasks-in-charge" class="form-label">Coloc' en charge</label>
              <select name="user-in-charge" class="form-control" id="new-task-in-charge" rows="3">
                <option value="">Choisissez un coloc'</option>
              </select>
              <label for="creation-date" class="form-label">Date de création</label>
              <input type="date" class="form-control" id="new-task-creation-date" rows="3">
              <button type="submit" class="btn btn-primary m-3 justify-self-center" id="create-tasks">CREER</button>
            </form>
          </div>
        </div>
      </div>
    </div>
`);
