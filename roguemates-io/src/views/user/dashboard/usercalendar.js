export default () => (`
    <div class="card card-shadow h-100">
      <h5 class="card-header"><i class="fa-regular fa-calendar-days"></i>  Calendrier</h5>
      <div class="card-body overflow-auto">
        <img src="https://blankcalendarpages.com/printable_calendar/calendrier1/janvier-2023-calendrier-fr1.jpg" class="card-img-top" alt="...">
      </div>
      <a href="/calendar" class="btn btn-primary ms-3 me-3 mb-3">Voir le calendrier</a>
    </div>
`);
