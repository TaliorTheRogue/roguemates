export default (userPayments) => (`
  <div class="card user-card card-shadow m-3">
    <h5 class="card-header"><i class="fa-solid fa-euro-sign"></i>  Mes Paiements</h5>
    <div class="card-body overflow-auto">
      <ul class="list-group overflow-auto">
        ${userPayments.map((userPayment) => `
          <li class="list-group-item">
            <input class="form-check-input me-1" type="checkbox" value="${userPayment.id}" id="task-${userPayment.id}" ${userPayment.done ? 'checked' : ''}>
            <label class="form-check-label" for="payment-${userPayment.id}">${userPayment.title} ${userPayment.money_sum} €</label>
          </li>
        `).join('')}
      </ul>
    </div>
    <button id="display-payments" class="btn btn-primary ms-3 me-3 mb-3">Voir les paiements</a>
  </div>

  <!-- Payments Modal -->
    <div id="payments-modal" class="modal">
      <div class="modal-content">
        <span class="close" id="close-payments-modal"><i class="fa-regular fa-circle-xmark"></i></span>
        <h2>NOS PAIEMENTS</h2>
        <div class="d-flex justify-content-between m-3 row">
          <div class="payments-table col-8">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Faite ?</th>
                  <th scope="col">Titre</th>
                  <th scope="col">Créée par</th>
                  <th scope="col">Montant</th>
                  <th scope="col">Date de création</th>
                  <th scope="col">Date limite</th>
                </tr>
              </thead>
              <tbody>
                ${userPayments.map((userPayment) => `
                  <tr>
                  <th scope="row">
                    <input class="form-check-input me-1" type="checkbox" value="${userPayment.id}" id="task-${userPayment.id}" ${userPayment.done ? 'checked' : ''}>
                  </th>
                  <td>${userPayment.title}</td>
                  <td>${userPayment.creator_id}</td>
                  <td>${userPayment.money_sum} €</td>
                  <td>${userPayment.creation_date}</td>
                  <td>${userPayments.limit_date}</td>
                </tr>
                `).join('')}
              </tbody>
            </table>
          </div>
          <div class="payment-create col-4">
            <p>NOUVEAU PAIEMENT :</p>
            <form class="payment-creation-form">
              <label for="title" class="form-label">Titre</label>
              <input class="form-control" id="new-payment-title" rows="3">
              <label for="category" class="form-label">Catégorie</label>
              <select name="payment-categories" class="form-control" id="payment-category-select">
                <option value="">Choisissez une catégorie</option>
                <option value="menage">Ménage</option>
                <option value="courses">Courses</option>
                <option value="loisirs">Loisirs</option>
                <option value="administratif">Administratif</option>
              </select>
              <label for="payments-money-sum" class="form-label">Montant €:</label>
              <input type="number" class="form-control" id="new-payment-limit-date" rows="3">
              <label for="description" class="form-label">Description</label>
              <textarea class="form-control" id="payments-description" rows="3"></textarea>
              <label for="payments-limit-date" class="form-label">Date limite</label>
              <input type="date" class="form-control" id="new-payment-limit-date" rows="3">
              <label for="tasks-in-charge" class="form-label">Coloc' en charge</label>
              <select name="user-in-charge" class="form-control" id="new-payment-in-charge" rows="3">
                <option value="">Choisissez un coloc'</option>
              </select>
              <label for="creation-date" class="form-label">Date de création</label>
              <input type="date" class="form-control" id="new-payment-creation-date" rows="3">
              <button type="submit" class="btn btn-primary m-3 justify-self-center" id="create-tasks">CREER</button>
            </form>
          </div>
        </div>
      </div>
    </div>
`);
