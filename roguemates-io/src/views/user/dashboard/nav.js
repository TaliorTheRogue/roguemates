import logo from '../../../assets/img/RogueMates Logo.png';

export default (userFirstName, colocationName) => (`
  <nav class="navbar bg-body-tertiary mb-1 ps-4 pe-4">
    <div class="container-fluid d-flex align-items-center">
      <a class="navbar-brand">
        <img src=${logo} alt="Logo" width="50" height="50" class="d-inline-block align-text-center text-light">
      </a>
      <h1 class="text-center">Bienvenue, ${userFirstName} !</h1>
      <h3>${colocationName}</h3>
    </div>
  </nav>
`);
