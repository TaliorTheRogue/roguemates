export default () => (`
  <div class="card text-center mt-3 mb-3 h-100 landlord-msg-card">
    <div class="card-header fw-bolder">
      <i class="fa-solid fa-circle-exclamation fa-2xl"></i>  MESSAGE DE VOTRE PROPRIETAIRE :  <i class="fa-solid fa-circle-exclamation fa-2xl"></i>
    </div>
    <div class="card-body">
      <p class="card-text fw-bold">Visite du plombier le 22/01/2024</p>
    </div>
  </div>
`);
