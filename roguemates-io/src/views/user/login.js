import Logo from '../../assets/img/RogueMates Logo.png';

export default () => (`
<section id="logform">
  <div class="homepage-login d-flex flex-column align-items-center">
    <div class="logo-space d-flex flex-column justify-content-center align-items-center">
      <img class="logo m-2" src=${Logo} alt="RogueMates Logo">
      <h1>RogueMates</h1>
      <h2 class="fst-italic">"La colocation en toute simplicité"</h2>
    </div>
    <form id="user-login-form" class="login-form d-flex flex-column justify-content-center w-50 h-50">
      <div class="m-3">
        <label for="email" class="form-label w-100 text-center">ADRESSE MAIL</label>
        <input type="email" class="form-control" id="user-email" placeholder="name@example.com">
      </div>
      <div class="m-3">
        <label for="password" class="form-label w-100 text-center">MOT DE PASSE</label>
        <input type="password" class="form-control" id="user-password" placeholder="Mot de passe">
      </div>
      <button type="submit" class="btn btn-primary m-3 justify-self-center" action="/dashboard">CONNEXION</button>
    </form>
    <a class="m-3" href="/landlordauth">Vous êtes propriétaire ? Cliquez ici !</a>
  </div>
<section>
`);
