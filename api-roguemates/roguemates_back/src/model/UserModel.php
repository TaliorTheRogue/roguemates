<?php

namespace App\Models;

use \PDO;
use stdClass;

class UserModel extends SqlConnect {
    public function add(array $data) {
      $query = "
        INSERT INTO users (first_name, last_name, pronoun, email, password, description, colocation_id)
        VALUES (:firstname, :lastname, :pronoun, :email, :password, :description, :colocationid)
      ";

      $req = $this->db->prepare($query);
      $req->execute($data);
    }

    public function delete(int $id) {
      $req = $this->db->prepare("DELETE FROM users WHERE id = :id");
      $req->execute(["id" => $id]);
    }

    public function get(int $id) {
      $req = $this->db->prepare("SELECT * FROM users WHERE id = :id");
      $req->execute(["id" => $id]);

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getLast() {
      $req = $this->db->prepare("SELECT * FROM users ORDER BY id DESC LIMIT 1");
      $req->execute();

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getAll() {
      $req = $this->db->prepare("SELECT * FROM users");
      $req->execute();

      return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getByEmail(string $email) {
      $req = $this->db->prepare("SELECT * FROM users WHERE email = :email");
      $req->execute(['email' => $email]);

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : null;
    }

  public function createSession($userId, $sessionToken) {
      $query = "
          INSERT INTO user_sessions (user_id, session_token, created_at)
          VALUES (:user_id, :session_token, NOW())
      ";
      $req = $this->db->prepare($query);
      $req->execute([
          "user_id" => $userId,
          "session_token" => $sessionToken
      ]);
  }

  public function getSession($sessionToken) {
      $req = $this->db->prepare("SELECT * FROM user_sessions WHERE session_token = :session_token");
      $req->execute(["session_token" => $sessionToken]);

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function deleteSession($sessionToken) {
      $req = $this->db->prepare("DELETE FROM user_sessions WHERE session_token = :session_token");
      $req->execute(["session_token" => $sessionToken]);
  }
}