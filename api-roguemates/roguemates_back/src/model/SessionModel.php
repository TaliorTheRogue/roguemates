<?php
namespace App\Models;

use PDO;

class SessionModel extends SqlConnect {
    public function createSession($userId, $sessionToken) {
        $expiresAt = date('Y-m-d H:i:s', strtotime('+7 days'));
        $query = "INSERT INTO user_sessions (user_id, session_token, expires_at) VALUES (:user_id, :session_token, :expires_at)";
        $stmt = $this->db->prepare($query);
        $stmt->execute([
            ':user_id' => $userId,
            ':session_token' => $sessionToken,
            ':expires_at' => $expiresAt
        ]);
    }

    public function getSession($sessionToken) {
        $query = "SELECT * FROM user_sessions WHERE session_token = :session_token AND expires_at > NOW()";
        $stmt = $this->db->prepare($query);
        $stmt->execute([':session_token' => $sessionToken]);
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function deleteSession($sessionToken) {
        $query = "DELETE FROM user_sessions WHERE session_token = :session_token";
        $stmt = $this->db->prepare($query);
        $stmt->execute([':session_token' => $sessionToken]);
    }
}
