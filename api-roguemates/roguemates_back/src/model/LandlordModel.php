<?php

namespace App\Models;

use \PDO;
use stdClass;

class LandlordModel extends SqlConnect {
    public function add(array $data) {
      $query = "
        INSERT INTO landlords (first_name, last_name, email, password)
        VALUES (:firstname, :lastname, :email, :password)
      ";

      $req = $this->db->prepare($query);
      $req->execute($data);
    }

    public function delete(int $id) {
      $req = $this->db->prepare("DELETE FROM landlords WHERE id = :id");
      $req->execute(["id" => $id]);
    }

    public function get(int $id) {
      $req = $this->db->prepare("SELECT * FROM landlords WHERE id = :id");
      $req->execute(["id" => $id]);

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getLast() {
      $req = $this->db->prepare("SELECT * FROM landlords ORDER BY id DESC LIMIT 1");
      $req->execute();

      return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
    }

    public function getAll() {
      $req = $this->db->prepare("SELECT * FROM landlords");
      $req->execute();

      return $req->fetchAll(PDO::FETCH_ASSOC);
    }
}
