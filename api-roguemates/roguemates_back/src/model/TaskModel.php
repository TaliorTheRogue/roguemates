<?php

namespace App\Models;

use \PDO;
use stdClass;

class TaskModel extends SqlConnect {
  public function add(array $data) {
    $query = "
      INSERT INTO tasks (category, title, description, creation_date, limit_date, user_id, done)
      VALUES (:category, :title, :description, :creationdate, :limitdate, :userid, :done)
    ";

    $req = $this->db->prepare($query);
    $req->execute($data);
  }

  public function delete(int $id) {
    $req = $this->db->prepare("DELETE FROM tasks WHERE id = :id");
    $req->execute(["id" => $id]);
  }

  public function get(int $id) {
    $req = $this->db->prepare("SELECT * FROM tasks WHERE id = :id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast() {
    $req = $this->db->prepare("SELECT * FROM tasks ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getAll() {
    $req = $this->db->prepare("SELECT * FROM tasks WHERE id=:id");
    $req->execute();

    return $req->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getTasksByUserId($userId) {
    $query = "SELECT * FROM tasks WHERE user_id = :userId";
    $stmt = $this->db->prepare($query);
    $stmt->execute([':userId' => $userId]);
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }

  public function updateStatus(int $taskId, bool $done) {
    $query = "UPDATE tasks SET done = :done WHERE id = :id";
    $req = $this->db->prepare($query);
    return $req->execute(['done' => $done, 'id' => $taskId]);
}
}
