<?php

namespace App\Models;

use \PDO;
use stdClass;

class PostitModel extends SqlConnect {
  public function add(array $data) {
    $query = "
      INSERT INTO post_its (title, message, user_author_id, creation_date)
      VALUES (:title, :message, :userauthorid, :creationdate)
    ";

    $req = $this->db->prepare($query);
    $req->execute($data);
  }

  public function delete(int $id) {
    $req = $this->db->prepare("DELETE FROM post_its WHERE id = :id");
    $req->execute(["id" => $id]);
  }

  public function get(int $id) {
    $req = $this->db->prepare("SELECT * FROM post_its WHERE id = :id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast() {
    $req = $this->db->prepare("SELECT * FROM post_its ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getAll() {
    $req = $this->db->prepare("SELECT * FROM post_its WHERE id=:id");
    $req->execute();

    return $req->fetchAll(PDO::FETCH_ASSOC);
  }
}
