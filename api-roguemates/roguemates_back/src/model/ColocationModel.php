<?php
namespace App\Models;

use \PDO;
use stdClass;

class ColocationModel extends SqlConnect {
  public function add(array $data) {
    $query = "
    INSERT INTO colocations (name, address, zip_code, city, landlord_id)
    VALUES (:name, :address, :zipcode, :city, :landlord)
    ";

    $req = $this->db->prepare($query);
    $req->execute($data);
  }

  public function delete(int $id) {
    $req = $this->db->prepare("DELETE FROM colocations WHERE id = :id");
    $req->execute(["id" => $id]);
  }

  public function get(int $id) {
    $req = $this->db->prepare("SELECT * FROM colocations WHERE id = :id");
    $req->execute(["id" => $id]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getLast() {
    $req = $this->db->prepare("SELECT * FROM colocations ORDER BY id DESC LIMIT 1");
    $req->execute();

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }

  public function getAll() {
    $req = $this->db->prepare("SELECT * FROM colocations WHERE id=:id");
    $req->execute();

    return $req->fetchAll(PDO::FETCH_ASSOC);
  }

  public function getByUserId(int $userId) {
    $query = "
    SELECT * 
    FROM colocations
    INNER JOIN users ON colocations.id = users.colocation_id
    WHERE users.id = :user_id
    ";

    $req = $this->db->prepare($query);
    $req->execute(['user_id' => $userId]);

    return $req->rowCount() > 0 ? $req->fetch(PDO::FETCH_ASSOC) : new stdClass();
  }
}