<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\SessionModel;
use App\Models\ColocationModel;

class User {
  protected array $params;
  protected string $reqMethod;
  protected object $model;
  protected object $sessionModel;

  public function __construct($params) {
    $this->params = $params;
    $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
    $this->model = new UserModel();
    $this->sessionModel = new SessionModel();

    $this->run();
  }

  protected function getUser() {
    if (isset($this->params['id'])) {
      $id = intval($this->params['id']);
      return $this->getSingleUser($id);
    }
    return $this->getUsers();
  }

  protected function getUsers() {
      return $this->model->getAll();
  }

  protected function getSingleUser(int $id) {
    $user = $this->model->get($id);
    if ($user) {
      $colocationModel = new ColocationModel();
      $colocation = $colocationModel->getByUserId($id);
      $user['colocation'] = $colocation;
      return [
        'status' => 'success',
        'user' => $user
      ];
    } else {
      return [
        'status' => 'error',
        'message' => 'User not found'
      ];
    }
  }

  protected function header() {
    header('Access-Control-Allow-Origin: http://127.0.0.1:9090');
    header('Content-type: application/json; charset=utf-8');
  }


  protected function ifMethodExist() {
    $method = $this->reqMethod.'User';

    if(empty($this->params['id'])) {
      $method .= 's';
    }

    if (method_exists($this, $method)) {
      echo json_encode($this->$method());

      return;
    }

    header('HTTP/1.0 404 Not Found');
    echo json_encode([
      'code' => '404',
      'message' => 'Not Found'
    ]);

    return;
  }

  protected function run() {
    $this->header();
    $this->ifMethodExist();
  }
}

