<?php

namespace App\Controllers;

use App\Models\LandlordModel;

class Landlord {
  protected array $params;
  protected string $reqMethod;
  protected object $model;

  public function __construct($params) {
    $this->params = $params;
    $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
    $this->model = new LandlordModel();

    $this->run();
  }

  protected function getLandlord() {
    if (isset($this->params['id'])) {
      return $this->getSingleLandlord($this->params['id']);
    }
    return $this->getLandlords();
  }

  protected function getLandlords() {
      return $this->model->getAll();
  }

  protected function getSingleLandlord(int $id) {
      return $this->model->get($id);
  }


  protected function header() {
    header('Access-Control-Allow-Origin: http://127.0.0.1:9090');
    header('Content-type: application/json; charset=utf-8');
  }


  protected function ifMethodExist() {
    $method = $this->reqMethod.'Landlord';

    if(empty($this->params['id'])) {
      $method .= 's';
    }

    if (method_exists($this, $method)) {
      echo json_encode($this->$method());

      return;
    }

    header('HTTP/1.0 404 Not Found');
    echo json_encode([
      'code' => '404',
      'message' => 'Not Found'
    ]);

    return;
  }

  protected function run() {
    $this->header();
    $this->ifMethodExist();
  }
}

