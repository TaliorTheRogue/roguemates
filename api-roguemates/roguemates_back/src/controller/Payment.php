<?php

namespace App\Controllers;

use App\Models\PaymentModel;

class Payment {
  protected array $params;
  protected string $reqMethod;
  protected object $model;

  public function __construct($params) {
    $this->params = $params;
    $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
    $this->model = new PaymentModel();

    $this->run();
  }

  protected function getPayments() {
    if (isset($this->params['user_id'])) {
      $userId = intval($this->params['user_id']);
      return $this->getPaymentsByUser($userId);
    }
    return $this->getAllPayments();
  }

  protected function getAllPayments() {
    $payments = $this->model->getAll();
    if (empty($payments)) {
      return [
        'status' => 'error',
        'message' => 'No payments found'
      ];
    }
    return [
        'status' => 'success',
        'payments' => $payments
    ];
  }

  protected function getPaymentsByUser(int $userId) {
    $payments = $this->model->getPaymentsByUserId($userId);
    if (empty($payments)) {
      return [
        'status' => 'error',
        'message' => 'No Payments found for this user'
      ];
    }
    return [
        'status' => 'success',
        'payments' => $payments
    ];
  }

  protected function header() {
    header('Access-Control-Allow-Origin: http://127.0.0.1:9090');
    header('Content-type: application/json; charset=utf-8');
  }

  protected function ifMethodExist() {
    $method = $this->reqMethod . 'Payments';

    if (method_exists($this, $method)) {
      echo json_encode($this->$method());
      return;
    }

    header('HTTP/1.0 404 Not Found');
    echo json_encode([
      'code' => '404',
      'message' => 'Not Found'
    ]);

    return;
  }

  protected function run() {
    $this->header();
    $this->ifMethodExist();
  }
}
