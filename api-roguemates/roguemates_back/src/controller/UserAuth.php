<?php
namespace App\Controllers;

use App\Models\UserModel;
use App\Models\SessionModel;

class UserAuth {
    protected array $params;
    protected string $reqMethod;
    protected object $userModel;
    protected object $sessionModel;

    public function __construct($params) {
        $this->params = $params;
        $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
        $this->userModel = new UserModel();
        $this->sessionModel = new SessionModel();

        $this->run();
    }

    // Log In Function
    protected function postUserAuth() {
        $data = json_decode(file_get_contents('php://input'), true);
        $email = $data['email'] ?? '';
        $password = $data['password'] ?? '';

        $user = $this->userModel->getByEmail($email);
        if ($user && $data['password'] === $user['password']) {
            $sessionToken = bin2hex(random_bytes(32));
            $this->sessionModel->createSession($user['id'], $sessionToken);

            return [
                'status' => 'success',
                'sessionToken' => $sessionToken,
                'userId' => $user['id']
            ];
        } else {
            return [
                'status' => 'error',
                'message' => 'Invalid email or password'
            ];
        }
    }

    // Log out Function
    protected function deleteUserAuth() {
        $sessionToken = $_SERVER['HTTP_AUTHORIZATION'] ?? '';
        $this->sessionModel->deleteSession($sessionToken);
        return [
            'status' => 'success',
            'message' => 'Logged out successfully'
        ];
    }

    protected function header() {
        header('Access-Control-Allow-Origin: http://127.0.0.1:9090');
        header('Content-type: application/json; charset=utf-8');
    }

    protected function ifMethodExist() {
        $method = $this->reqMethod . 'UserAuth';

        if (method_exists($this, $method)) {
            echo json_encode($this->$method());

            return;
        }

        header('HTTP/1.0 404 Not Found');
        echo json_encode([
            'code' => '404',
            'message' => 'Not Found'
        ]);

        return;
    }

    protected function run() {
        $this->header();
        $this->ifMethodExist();
    }
}

