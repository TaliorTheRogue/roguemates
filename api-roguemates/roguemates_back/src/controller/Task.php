<?php

namespace App\Controllers;

use App\Models\TaskModel;

class Task {
  protected array $params;
  protected string $reqMethod;
  protected object $model;

  public function __construct($params) {
    $this->params = $params;
    $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
    $this->model = new TaskModel();

    $this->run();
  }

  protected function getTasks() {
    if (isset($this->params['user_id'])) {
      $userId = intval($this->params['user_id']);
      return $this->getTasksByUser($userId);
    }
    return $this->getAllTasks();
  }

  protected function getAllTasks() {
    $tasks = $this->model->getAll();
    if (empty($tasks)) {
      return [
        'status' => 'error',
        'message' => 'No tasks found'
      ];
    }
    return [
        'status' => 'success',
        'tasks' => $tasks
    ];
  }

  protected function getTasksByUser(int $userId) {
    $tasks = $this->model->getTasksByUserId($userId);
    if (empty($tasks)) {
      return [
        'status' => 'error',
        'message' => 'No tasks found for this user'
      ];
    }
    return [
        'status' => 'success',
        'tasks' => $tasks
    ];
  }

  protected function updateTaskStatus() {
    $taskId = intval($this->params['task_id']);
    $data = json_decode(file_get_contents('php://input'), true);
    $done = $data['done'] ?? null;

    if ($done === null) {
        return [
            'status' => 'error',
            'message' => 'Status not provided'
        ];
    }

    $updated = $this->model->updateStatus($taskId, $done);

    if ($updated) {
        return [
            'status' => 'success',
            'message' => 'Task status updated'
        ];
    } else {
        return [
            'status' => 'error',
            'message' => 'Failed to update task status'
        ];
    }
}

  protected function header() {
    header('Access-Control-Allow-Origin: http://127.0.0.1:9090');
    header('Content-type: application/json; charset=utf-8');
  }

  protected function ifMethodExist() {
    $method = $this->reqMethod . 'Tasks';

    if (!empty($this->params['task_id']) && $this->reqMethod === 'put') {
      $method = 'updateTaskStatus';
    }

    if (method_exists($this, $method)) {
      echo json_encode($this->$method());
      return;
    }

    header('HTTP/1.0 404 Not Found');
    echo json_encode([
      'code' => '404',
      'message' => 'Not Found'
    ]);

    return;
  }

  protected function run() {
    $this->header();
    $this->ifMethodExist();
  }
}
