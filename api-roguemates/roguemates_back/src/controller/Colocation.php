<?php

namespace App\Controllers;

use App\Models\ColocationModel;
use App\Models\SessionModel;

class Colocation {
  protected array $params;
  protected string $reqMethod;
  protected object $model;
  protected object $sessionModel;

  public function __construct($params) {
    $this->params = $params;
    $this->reqMethod = strtolower($_SERVER['REQUEST_METHOD']);
    $this->model = new ColocationModel();
    $this->sessionModel = new SessionModel();

    $this->run();
  }

  protected function getColocation() {
    if (isset($this->params['id'])) {
      $id = intval($this->params['id']);
      return $this->getSingleColocation($id);
    }
    return $this->getColocations();
  }

  protected function getColocations() {
      return $this->model->getAll();
  }

  protected function getSingleColocation(int $id) {
    $user = $this->model->get($id);
    if ($user) {
        return [
            'status' => 'success',
            'user' => $user
        ];
    } else {
        return [
            'status' => 'error',
            'message' => 'User not found'
        ];
    }
  }

  protected function getColocByUserId(int $id) {
    
    
  }

  protected function header() {
    header('Access-Control-Allow-Origin: http://127.0.0.1:9090');
    header('Content-type: application/json; charset=utf-8');
  }


  protected function ifMethodExist() {
    $method = $this->reqMethod.'Colocation';

    if(empty($this->params['id'])) {
      $method .= 's';
    }

    if (method_exists($this, $method)) {
      echo json_encode($this->$method());

      return;
    }

    header('HTTP/1.0 404 Not Found');
    echo json_encode([
      'code' => '404',
      'message' => 'Not Found'
    ]);

    return;
  }

  protected function run() {
    $this->header();
    $this->ifMethodExist();
  }
}
