<?php

header('Access-Control-Allow-Origin: http://127.0.0.1:9090');
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type, Authorization');
header('Access-Control-Allow-Credentials: true');

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
    header('HTTP/1.1 200 OK');
    exit;
}

require 'vendor/autoload.php';

use App\Controllers\Landlord;
use App\Controllers\Payment;
use App\Router;
use App\Controllers\User;
use App\Controllers\UserAuth;
use App\Controllers\Task;

$routes = [
  'user/:id' => User::class,
  'users' => User::class,
  'user/:user_id/tasks' => Task::class,
  'tasks/:task_id' => Task::class,
  'user/:user_id/payments' => Payment::class,
  'landlord/:id' => Landlord::class,
  'landlords' => Landlord::class,
  'login' => UserAuth::class,
  'logout' => UserAuth::class
];

$router = new Router($routes);

