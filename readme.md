# RogueMates

RogueMates est une application web qui a pour but de permettre une gestion fluide d'une ou de plusieurs colocations,
tant pour le propriétaire que pour les colocataires.

Il y a donc une partie colocataire, accessible en se connectant sur la page d'accueil avec l'un des deux identifiants suivants :
- Email : alex.taylor@discord.gg Password : Agent45
- Email : jake.crimson@discord.gg Password : 123456
Une connexion réussi vous emmènera vers le dashboard colocataire qui vous permet d'avoir une vue d'ensemble.

Il y a également une partie propriétaire qui n'est pas encore fonctionnelle mais qui est accessible via l'URL.
Il vous suffit de taper en fin d'URL :
- /landlordauth => Pour accéder au login et register des propriétaires
- /landlorddashboard => Pour accéder au dashboard propriétaire

## Prérequis

Liste des logiciels et outils nécessaires pour exécuter le projet.

- Node.js (version 14 ou supérieure)
- PHP (version 7.4 ou supérieure)
- MySQL
- Nginx
- Composer

## Installation

Instructions étape par étape pour installer les dépendances et configurer le projet.

### Configuration du Serveur

1. **Cloner le dépôt :**

    ```bash
    git clone https://github.com/username/projet.git
    cd projet
    ```

2. **Installer les dépendances PHP avec Composer :**

    ```bash
    composer install
    ```

3. **Installer les dépendances Node.js :**

    ```bash
    npm install
    ```

4. **Configurer la base de données :**

  Importer le fichier 'roguemates.sql' via phpmyadmin.

### Configuration de Nginx

1. **Configurer un hôte virtuel Nginx :**

    Créez un fichier de configuration Nginx pour votre projet dans `/etc/nginx/sites-available/` :

    ```nginx
    server {
        listen 80;
        server_name example.com; # Remplacez par votre nom de domaine ou adresse IP

        root /path/to/projet/public; # Remplacez par le chemin absolu vers le répertoire public de votre projet
        index index.php index.html index.htm;

        location / {
            try_files $uri $uri/ /index.php?$query_string;
        }

        location ~ \.php$ {
            include snippets/fastcgi-php.conf;
            fastcgi_pass unix:/var/run/php/php7.4-fpm.sock; # Remplacez par votre version de PHP
            fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
            include fastcgi_params;
        }

        location ~ /\.ht {
            deny all;
        }
    }
    ```

2. **Activer la configuration Nginx :**

    ```bash
    sudo ln -s /etc/nginx/sites-available/votre-configuration /etc/nginx/sites-enabled/
    sudo nginx -t
    sudo systemctl reload nginx
    ```

### Démarrer le Back-end

```bash
sudo systemctl start nginx
sudo systemctl start php7.4-fpm
```


### Démarrer le Front-end

```bash
npm start
```

## Licence

Ce projet est sous licence MIT.